package com.devcamp.customeraccountrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerAccountRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerAccountRestApiApplication.class, args);
	}

}
