package com.devcamp.customeraccountrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountrestapi.model.Customer;
import com.devcamp.customeraccountrestapi.service.CustomerService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = customerService.getAllCustomer();
        return allCustomer;
    }

}
