package com.devcamp.customeraccountrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountrestapi.model.Account;
import com.devcamp.customeraccountrestapi.service.AccountService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccount() {
        ArrayList<Account> allAccount = accountService.getAllAccount();
        return allAccount;
    }

}
