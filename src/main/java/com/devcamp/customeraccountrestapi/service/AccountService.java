package com.devcamp.customeraccountrestapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customeraccountrestapi.model.Account;

@Service
public class AccountService {

    @Autowired
    private CustomerService customerService;

    Account NganHangACB = new Account(1222200, 100000000);
    Account NganHangMBB = new Account(1222200, 200000000);
    Account NganHangVIB = new Account(1222200, 300000000);
    Account NganHangBIDV = new Account(1222200, 400000000);
    Account NganHangAGR = new Account(1222200, 500000000);
    Account NganHangTEC = new Account(1222200, 600000000);

    public ArrayList<Account> getAllAccount() {
        ArrayList<Account> allAccount = new ArrayList<>();
        NganHangACB.setCustomer(customerService.KhachHangACB);
        NganHangMBB.setCustomer(customerService.KhachHangMBB);
        NganHangVIB.setCustomer(customerService.KhachHangVIB);
        NganHangBIDV.setCustomer(customerService.KhachHangBIDV);
        NganHangAGR.setCustomer(customerService.KhachHangARI);
        NganHangTEC.setCustomer(customerService.KhachHangTEC);

        allAccount.add(NganHangACB);
        allAccount.add(NganHangMBB);
        allAccount.add(NganHangVIB);
        allAccount.add(NganHangBIDV);
        allAccount.add(NganHangAGR);
        allAccount.add(NganHangTEC);
        return allAccount;

    }

}
