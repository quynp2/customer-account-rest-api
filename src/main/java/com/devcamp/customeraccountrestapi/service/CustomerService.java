package com.devcamp.customeraccountrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccountrestapi.model.Customer;

@Service
public class CustomerService {

    Customer KhachHangACB = new Customer(12222, "Ngo Thi Hoang Nghia", 30);
    Customer KhachHangMBB = new Customer(9999, "Ngo Tich", 41);
    Customer KhachHangTEC = new Customer(66666, "Ngo Thien Quang", 33);
    Customer KhachHangVIB = new Customer(34563, "Ngo Van Hoang Phuc", 54);
    Customer KhachHangBIDV = new Customer(68888, "Huynh Thi Bong", 40);
    Customer KhachHangARI = new Customer(899998, "Vo Thi Hoa", 40);

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = new ArrayList<>();
        allCustomer.add(KhachHangACB);
        allCustomer.add(KhachHangMBB);
        allCustomer.add(KhachHangTEC);
        allCustomer.add(KhachHangVIB);
        allCustomer.add(KhachHangBIDV);
        allCustomer.add(KhachHangARI);
        return allCustomer;

    }

}
